package validator.password.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import validator.password.exception.InvalidPasswordException;
import validator.password.resource.request.PasswordRequest;

@Service
public class PasswordService {

	public boolean validate(PasswordRequest request) throws InvalidPasswordException {
		String password = request.getPassword();

		validateLength(password);
		validateCaseAndNumeric(password);
		validateSequence(password);
		return true;
	}

	public boolean validateLength(String password) throws InvalidPasswordException {
		if (password.length() < 5 || password.length() > 12) {
			throw new InvalidPasswordException("Password must be between 5 and 12 characters in length.");
		}
		return true;
	}

	public boolean validateCaseAndNumeric(String password) throws InvalidPasswordException {
		String regex = "^(?=.*\\d)(?=.*[a-z])[a-z0-9]*$";

		Pattern pattern1 = Pattern.compile(regex);
		Matcher matcher1 = pattern1.matcher(password);
		if (!matcher1.matches()) {
			throw new InvalidPasswordException("Password Must consist of a mixture of lowercase letters and numerical digits only, with at least one of each.");
		}
		return true;
	}

	public boolean validateSequence(String password) throws InvalidPasswordException {
		String regex = "(?!(.+?)\\1).*";

		Pattern pattern1 = Pattern.compile(regex);
		Matcher matcher1 = pattern1.matcher(password);
		if (!matcher1.matches()) {
			throw new InvalidPasswordException("Password must not contain any sequence of characters immediately followed by the same sequence.");
		}
		return true;
	}

}