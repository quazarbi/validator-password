package validator.password;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan
@EntityScan
@EnableSwagger2
public class ValidatorPasswordApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidatorPasswordApplication.class, args);
	}
}
