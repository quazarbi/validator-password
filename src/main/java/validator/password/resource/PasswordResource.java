package validator.password.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import validator.password.exception.InvalidPasswordException;
import validator.password.resource.request.PasswordRequest;
import validator.password.service.PasswordService;

@RestController
@RequestMapping("/password")
public class PasswordResource {
	
	@Autowired
	private PasswordService passwordService;
	
	@PostMapping(path = "/validate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Boolean> validate(@RequestBody PasswordRequest passwordRequest) {
		boolean validationResponse = true;
		
		try {
			passwordService.validate(passwordRequest);
		} catch (InvalidPasswordException ipe) {
			validationResponse = false;
		}
		return new ResponseEntity<>(validationResponse, HttpStatus.OK);
	}
	
}
