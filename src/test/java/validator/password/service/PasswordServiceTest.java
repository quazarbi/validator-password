package validator.password.service;

import org.junit.Assert;
import org.junit.Test;

import validator.password.exception.InvalidPasswordException;
import validator.password.resource.request.PasswordRequest;

public class PasswordServiceTest {
	
	@Test(expected = InvalidPasswordException.class)
	public void testLength_LessThan5() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		service.validateLength("test");
	}
	
	@Test(expected = InvalidPasswordException.class)
	public void testLength_GreaterThan12() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		service.validateLength("verylongpassword");
	}
	
	@Test
	public void testLength_Valid() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		boolean valid = service.validateLength("password");
		Assert.assertTrue(valid);
	}
	
	@Test(expected = InvalidPasswordException.class)
	public void testCaseNumber_NoNumbers() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		service.validateCaseAndNumeric("invalidpwd");
	}
	
	@Test(expected = InvalidPasswordException.class)
	public void testCaseNumber_NoLetters() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		service.validateCaseAndNumeric("123456");
	}
	
	@Test
	public void testCaseNumber_Valid() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		boolean valid = service.validateCaseAndNumeric("pwd123");
		Assert.assertTrue(valid);
	}
	
	@Test(expected = InvalidPasswordException.class)
	public void testCaseNumber_Uppercase() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		service.validateCaseAndNumeric("PWD123");
	}
	
	@Test(expected = InvalidPasswordException.class)
	public void testSequence_Repeating() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		service.validateSequence("pwdpwd");
	}
	
	@Test
	public void testSequence_NonRepeating() throws InvalidPasswordException {
		PasswordService service = new PasswordService();
		boolean valid = service.validateSequence("pwd123");
		Assert.assertTrue(valid);
	}
	
	@Test
	public void testValidate() throws InvalidPasswordException {
		PasswordRequest request = new PasswordRequest();
		request.setPassword("pwd123");
		
		PasswordService service = new PasswordService();
		boolean valid = service.validate(request);
		Assert.assertTrue(valid);
	}

}
